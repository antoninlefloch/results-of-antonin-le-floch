Here are the results of tests to compare QUIC and TCP and especially the multipath version of these protocols. 
 
**The tests compare TCP, MPTCP, QUIC and MPQUIC over Retransmission, Errors and Delay.**

The results are presented in a G-Sheet here : https://docs.google.com/spreadsheets/d/1nDl32_ka8BzjGgJzZndr94Ijm5qWgxkZGo0QVPipetw/edit?usp=sharing
 
 
The discussion about the results will be detailed in a paper soon. 
 
 
This project is a contribution to the Fernando Nakayama's work. 
It is based on the work of Quentin De Coninck's work on MPQUIC (link : https://multipath-quic.org/2017/12/09/artifacts-available.html)
 
 

